**Database Description:**

This database consists of 4 tables:

  - *tickers* - contains 1 column which displays existing tickers. This column is a primary key in this table and foreign key in all other tables.
  - *live* - contains real time data for tickers.
  - *extensive* - contains full-scale description for tickers.
  - *historical* - contains historical data for tickers.

Table relationships:

  *tickers* -> *live* (one-to-one); *tickers* -> *extensive* (one-to-one); *tickers* -> *historical* (one-to-many)

*tickers* table stays in sync with all the other tables. If the row is deleted in that table, it will automatically remove the data for this particular ticker in *live*, *extensive* and *historical* tables. Vice-versa, API calls that populates data in *live*, *extensive* and *historical* tables will create a row in *tickers* table. Hence, the main purpose of the *tickers* table is to provide quick and efficient lookup for tickers that are stored in the database.



**FAQ:**

  1) How do I get all the tickers from the database?

  SQL Query: SELECT * FROM tickers


  2) How do I get real time data for all tickers?

  SQL Query: SELECT * FROM live


  3) How do I get extensive data for all tickers?

  SQL Query: SELECT * FROM extensive


  4) How do I get historical data for a particular ticker?

  SQL Query: SELECT * FROM historical WHERE ticker='*ticker_name*'


  5) How can I check the best performing tickers?

  SQL Query: SELECT * FROM extensive ORDER BY ticker_changes_perc DESC


  6) How can I check the best performing tickers, sorted by their industries?

  SQL Query: SELECT * FROM extensive ORDER BY sector ASC, ticker_changes_perc DESC


  7) How can I get the biggest change in price amongst all tickers?

  SQL Query: SELECT * FROM historical ORDER BY price_change ASC


  8) How can I check the average number of shares traded within a day amongst all the tickers?

  SQL Query: SELECT * FROM extensive order by trade_volume_average DESC;


  9) How can I check total market value of a company's outstanding shares?

  SQL Query: SELECT * FROM extensive order by market_capitalisation DESC;


  10) How can I get all prices of the stocks within the range of dates (04/07/2007 to 04/07/2008);

  SQL Query: SELECT * FROM historical WHERE ticker_data_date >= '20070704000000' AND ticker_data_date <= '20080704000000';
