DROP DATABASE IF EXISTS stocks;

CREATE DATABASE stocks;

USE stocks;

DROP TABLE IF EXISTS tickers;
DROP TABLE IF EXISTS live;
DROP TABLE IF EXISTS extensive;
DROP TABLE IF EXISTS historical;

CREATE TABLE tickers
(
	ticker varchar(10)	NOT NULL,
	PRIMARY KEY(ticker)
);

CREATE TABLE live
(
	id	int	NOT NULL AUTO_INCREMENT,
	ticker	varchar(10)	NOT NULL,
	price	decimal(15,2)	NOT NULL,
	date_and_time	datetime	NOT NULL,
	FOREIGN KEY(ticker) REFERENCES tickers(ticker),
	PRIMARY KEY(id)
);

CREATE TABLE extensive
(
	id	int	NOT NULL AUTO_INCREMENT,
    ticker	varchar(10)	NOT NULL,
	price	decimal(15,2)	NOT NULL,
	trade_volume_average	varchar(25)	NOT NULL,
	market_capitalisation	varchar(25)	NOT NULL,
	last_div	varchar(25)	NOT NULL,
	ticker_range	varchar(25)	NOT NULL,
	ticker_changes	decimal(15,2)	NOT NULL,
	ticker_changes_perc	varchar(25)	NOT NULL,
	company_name	varchar(25)	NOT NULL,
	ticker_exchange	varchar(25)	NOT NULL,
	industry	varchar(25)	NOT NULL,
	website	varchar(50)	NOT NULL,
	company_description	varchar(250)	NOT NULL,
	company_ceo	varchar(50)	NOT NULL,
	sector	varchar(50)	NOT NULL,
	company_image	varchar(250)	NOT NULL,
	FOREIGN KEY(ticker) REFERENCES tickers(ticker),
	PRIMARY KEY(id)
);

CREATE TABLE historical
(
	id	int	NOT NULL AUTO_INCREMENT,
    ticker	varchar(10)	NOT NULL,
	ticker_data_date datetime NOT NULL,
	open_price decimal(15,4)	NOT NULL,
	low_price decimal(15,4)	NOT NULL,
	close_price decimal(15,4)	NOT NULL,
	trade_volume decimal(25,4)	NOT NULL,
	unadjusted_trade_volume decimal(25,4)	NOT NULL,
	price_change decimal(15,6) NOT NULL,
	price_change_percentage decimal(15,4) NOT NULL,
	vwap decimal(15,4) NOT NULL,
	label varchar(25) NOT NULL,
	price_change_ot decimal(15,4) NOT NULL,
	FOREIGN KEY(ticker) REFERENCES tickers(ticker),
	PRIMARY KEY(id)
);

USE stocks;

INSERT INTO tickers (ticker)
VALUES
("AAPL"),
("GOOGL"),
("NFLX"),
("FB"),
("MSFT");


INSERT INTO live (ticker, price, date_and_time)
VALUES
("AAPL", 220.63, 20060704000000),
("GOOGL", 230.12, 20060704000000),
("NFLX", 190.78, 20060704000000),
("FB", 209.12, 20060704000000),
("MSFT", 212.23, 20060704000000);

INSERT INTO extensive (ticker, price, trade_volume_average, market_capitalisation, last_div, ticker_range, ticker_changes, ticker_changes_perc, company_name, ticker_exchange, industry, website, company_description, company_ceo, sector, company_image)
VALUES
("AAPL", 220.63, "36724977", "1113174266670.00", "2.92", "142-233.47", "0.59", "(+0.27%)", "Apple Inc.", "Nasdaq Global Select", "Computer Hardware", "http://www.apple.com", "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.", "Timothy D.", "Technology", "https://financialmodelingprep.com/images-New-jpg/AAPL.jpg"),
("GOOGL", 230.12, "31224977", "2113123266670.00", "2.88", "143-233.47", "0.29", "(+0.17%)", "Google Inc.", "Nasdaq Global Select", "Computer Hardware", "http://www.google.com", "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.", "Timothy B. Cook", "Technology", "https://financialmodelingprep.com/images-New-jpg/GOOGL.jpg"),
("NFLX", 190.78, "31224921", "1113174266670.00", "2.62", "144-233.47", "0.53", "(+0.22%)", "Netflix Inc.", "Nasdaq Global Select", "Computer Hardware", "http://www.netflix.com", "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.", "Timothy C. Cook", "Technology", "https://financialmodelingprep.com/images-New-jpg/NFLX.jpg"),
("FB", 209.12, "33424977", "2122174266670.00", "2.72", "141-233.47", "0.66", "(+0.23%)", "Facebook Inc.", "Nasdaq Global Select", "Computer Hardware", "http://www.facebook.com", "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.", "Timothy E. Cook", "Technology","https://financialmodelingprep.com/images-New-jpg/FB.jpg"),
("MSFT", 212.23, "32344977", "2333174266670.00", "2.32", "143-533.47", "0.52", "(+0.37%)", "Microsoft Inc.", "Nasdaq Global Select", "Computer Hardware", "http://www.facebook.com", "Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.", "Timothy F. Cook", "Technology", "https://financialmodelingprep.com/images-New-jpg/MSFT.jpg");

INSERT INTO historical (ticker, ticker_data_date, open_price, low_price, close_price, trade_volume, unadjusted_trade_volume, price_change, price_change_percentage, vwap, label, price_change_ot)
VALUES
("AAPL", 20070704000000, 221.63, 719.34, 808.78, 6.278559, 6.8787457, 0.94, 0.465, 48.776, 44, 0.1),
("AAPL", 20080704000000, 221.63, 719.34, 808.78, 6.278559, 6.8787457, 0.94, 0.465, 48.776, 44, 0.1),
("GOOGL", 20060704000000, 220.63, 619.34, 738.78, 5.138559, 6.6787457, 0.91, 0.455, 49.776, 46, 0.1),
("NFLX", 20060704000000, 261.63, 789.34, 678.78, 6.123559, 6.4487457, 0.92, 0.475, 47.776, 42, 0.1),
("FB", 20060704000000, 190.63, 830.34, 820.78, 6.271239, 6.7787457, 0.93, 0.445, 46.176, 41, 0.1),
("MSFT", 20060704000000, 121.63, 719.34, 818.78, 6.123559, 5.8787457, 0.96, 0.565, 42.776, 47, 0.1);